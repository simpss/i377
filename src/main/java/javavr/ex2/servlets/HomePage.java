package javavr.ex2.servlets;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/HomePage")
public class HomePage extends HttpServlet {

    public static final String PARAM_KEY = "param";

    private static final String JSP_FILE = "/WEB-INF/ex2/HomePage.jsp";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getParameter(PARAM_KEY) != null) {
            request.getSession().setAttribute(PARAM_KEY, request.getParameter(PARAM_KEY));
        }

        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(JSP_FILE);
        dispatcher.forward(request, response);
    }
}
