package javavr.ex2.servlets;

import javavr.ex2.listeners.SessionCountListener;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/SessionCount")
public class SessionCount extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        int count = SessionCountListener.getCount();
        resp.getWriter().println(String.format("Session count: %s", count));
    }
}
