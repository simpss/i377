package javavr.ex3;


public class Ex3Context {
    public final static String JSP_FOLDER = "/WEB-INF/ex3/%s";
    public final static String RESOURCE_FOLDER = "../../ex3/";
    public final static String ENCODING = "UTF-8";

    public final static String DB_CONNECTION_STRING = "jdbc:hsqldb:mem:customerDB";
    public final static String DB_USR = "sa";
    public final static String DB_PSWD = "";

    public final static String ACTION_KEY = "action";
    public final static String ACTION_DEL_ID = "delById";
    public final static String KEY_ID = "id";
    public final static String ACTION_DEL_ALL = "deleteall";
    public final static String ACTION_ADD_SAMPLE_DATA = "sampledata";
}
