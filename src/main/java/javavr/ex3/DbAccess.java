package javavr.ex3;

import java.io.IOException;
import java.io.InputStream;
import java.sql.*;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.io.IOUtils;


/**
 * Singleton class to access the db.
 */
public class DbAccess {
    private static DbAccess dbAccess = new DbAccess();

    /**
     * An empty private constructor so this class couldn't be instatiated.
     */
    private DbAccess() {
    }

    /**
     * a method to access the singleton
     *
     * @return DbAccess singleton.
     */
    public static DbAccess getInstance() {
        return dbAccess;
    }

    /**
     * Load the hsqldb driver.
     */
    static {
        try {
            Class.forName("org.hsqldb.jdbcDriver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public void createTable() throws SQLException {
        String sql = readResource("ex3/create_table.sql");
        executeSql(sql);
    }

    public void dropSchema() throws SQLException {
        executeSql("DROP SCHEMA PUBLIC CASCADE");
    }

    public void createTestData() throws SQLException {
        executeSql(readResource("ex3/insert_data.sql"));
    }

    public Connection getConnection() throws SQLException {
        return DriverManager.getConnection(Ex3Context.DB_CONNECTION_STRING, Ex3Context.DB_USR, Ex3Context.DB_PSWD);
    }

    private void executeSql(String sql) throws SQLException {
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        statement.executeUpdate(sql);

        DbUtils.closeQuietly(connection);
        DbUtils.closeQuietly(statement);
    }

    private String readResource(String filename) {
        InputStream input = getClass().getResourceAsStream(Ex3Context.RESOURCE_FOLDER + filename);
        try {
            return IOUtils.toString(input, Ex3Context.ENCODING);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            IOUtils.closeQuietly(input);
        }
    }
}
