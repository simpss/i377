package javavr.ex3.models;

import javavr.ex3.models.impl.Customer;
import javavr.ex3.models.impl.CustomerDaoImpl;

import java.sql.SQLException;
import java.util.ArrayList;

public interface CustomerDao {

    ArrayList<Customer> getAllCustomers() throws SQLException;

    ArrayList<Customer> getFilteredCustomers(String searchString) throws SQLException;

    void deleteCustomer(int id) throws SQLException;

    void addCustomer(Customer customer) throws SQLException;
}
