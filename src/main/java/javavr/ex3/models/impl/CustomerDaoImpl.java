package javavr.ex3.models.impl;

import javavr.ex3.DbAccess;
import javavr.ex3.models.CustomerDao;
import org.apache.commons.dbutils.DbUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class CustomerDaoImpl implements CustomerDao {

    private static CustomerDao instance = new CustomerDaoImpl();

    private CustomerDaoImpl() {

    }

    public static CustomerDao getInstance() {
        return instance;
    }

    @Override
    public ArrayList<Customer> getAllCustomers() throws SQLException {
        Connection con = DbAccess.getInstance().getConnection();
        PreparedStatement statement = con.prepareStatement("SELECT * FROM customer");

        return queryCustomerByStatement(con, statement);
    }

    @Override
    public ArrayList<Customer> getFilteredCustomers(String searchString) throws SQLException {
        Connection con = DbAccess.getInstance().getConnection();
        PreparedStatement statement = con.prepareStatement("SELECT * FROM customer WHERE UPPER(first_name) LIKE ? OR UPPER(surname) LIKE ?");
        statement.setString(1, searchString.toUpperCase());
        statement.setString(2, searchString.toUpperCase());
        return queryCustomerByStatement(con, statement);
    }

    @Override
    public void deleteCustomer(int id) throws SQLException {
        Connection con = DbAccess.getInstance().getConnection();
        PreparedStatement statement = con.prepareStatement("DELETE FROM customer WHERE id = ?");
        statement.setInt(1, id);

        statement.executeUpdate();
        DbUtils.closeQuietly(con);
        DbUtils.closeQuietly(statement);
    }

    @Override
    public void addCustomer(Customer customer) throws SQLException {
        Connection con = DbAccess.getInstance().getConnection();
        PreparedStatement statement = con.prepareStatement(
                "INSERT INTO customer (id, first_name, surname, code) VALUES (NEXT VALUE FOR seq1, ?, ?, ?)");
        statement.setString(1, customer.getFirstName());
        statement.setString(2, customer.getSurName());
        statement.setString(3, customer.getCode());
        statement.executeUpdate();

        DbUtils.closeQuietly(con);
        DbUtils.closeQuietly(statement);
    }

    private ArrayList<Customer> queryCustomerByStatement(Connection con, PreparedStatement statement) throws SQLException {
        ArrayList<Customer> customers = new ArrayList<Customer>();
        ResultSet result = statement.executeQuery();
        while (result.next()) {
            customers.add(
                    toCustomer(result.getInt(1), result.getString(2), result.getString(3), result.getString(4)));
        }
        DbUtils.closeQuietly(con);
        DbUtils.closeQuietly(statement);
        DbUtils.closeQuietly(result);

        return customers;
    }

    private Customer toCustomer(int id, String firstName, String surName, String code) {
        Customer customer = new Customer();
        customer.setId(id);
        customer.setFirstName(firstName);
        customer.setSurName(surName);
        customer.setCode(code);
        return customer;
    }
}
