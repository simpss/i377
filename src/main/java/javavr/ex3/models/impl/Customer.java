package javavr.ex3.models.impl;

public class Customer {
    private int id;
    private String firstName;
    private String surName;
    private String code;

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSurName() {
        return surName;
    }

    public String getCode() {
        return code;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
