package javavr.ex3.servlets;

import javavr.ex3.Ex3Context;
import javavr.ex3.models.CustomerDao;
import javavr.ex3.models.impl.Customer;
import javavr.ex3.models.impl.CustomerDaoImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

@WebServlet("/ex3/Search")
public class Search extends HttpServlet {

    public static final String SERVLET_URI = "%s/Search";
    public static final String KEY_CUSTOMERS = "customers";
    public static final String KEY_SEARCH = "searchString";
    private static final String JSP_FILE = String.format(Ex3Context.JSP_FOLDER, "Search.jsp");

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ArrayList<Customer> customers = new ArrayList<Customer>();
        CustomerDao dao = CustomerDaoImpl.getInstance();

        String searchString = request.getParameter(KEY_SEARCH);

        try {
            if (searchString == null || searchString.isEmpty()) {
                customers.addAll(dao.getAllCustomers());
            } else {
                customers.addAll(dao.getFilteredCustomers(searchString));
            }
        } catch (SQLException e) {
            response.getWriter().write("sql error: " + e.getErrorCode());
        }


        request.setAttribute(KEY_CUSTOMERS, customers);
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(JSP_FILE);
        dispatcher.forward(request, response);
    }
}
