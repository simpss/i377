package javavr.ex3.servlets;

import javavr.ex3.DbAccess;
import javavr.ex3.Ex3Context;
import javavr.ex3.models.CustomerDao;
import javavr.ex3.models.impl.Customer;
import javavr.ex3.models.impl.CustomerDaoImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/ex3/Add")
public class Add extends HttpServlet {

    public static final String KEY_FIRST_NAME = "firstname";
    public static final String KEY_SUR_NAME = "surname";
    public static final String KEY_CODE = "code";

    private static final String JSP_FILE = String.format(Ex3Context.JSP_FOLDER, "Add.jsp");


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (!doAction(req, resp)) {
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(JSP_FILE);
            dispatcher.forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        CustomerDao dao = CustomerDaoImpl.getInstance();

        Customer customer = new Customer();
        customer.setFirstName(req.getParameter(KEY_FIRST_NAME));
        customer.setSurName(req.getParameter(KEY_SUR_NAME));
        customer.setCode(req.getParameter(KEY_CODE));

        try {
            dao.addCustomer(customer);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        resp.sendRedirect(String.format(Search.SERVLET_URI, req.getContextPath()));
    }

    private boolean doAction(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String action = req.getParameter(Ex3Context.ACTION_KEY);
        if (action != null) {
            try {
                if (action.equals(Ex3Context.ACTION_ADD_SAMPLE_DATA)) {
                    DbAccess.getInstance().createTestData();
                } else if (action.equals(Ex3Context.ACTION_DEL_ALL)) {
                    DbAccess.getInstance().dropSchema();
                    DbAccess.getInstance().createTable();
                } else if (action.equals(Ex3Context.ACTION_DEL_ID)) {
                    int id = Integer.parseInt(req.getParameter(Ex3Context.KEY_ID));
                    CustomerDaoImpl.getInstance().deleteCustomer(id);
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
            resp.sendRedirect(String.format(Search.SERVLET_URI, req.getContextPath()));
            return true;
        }
        return false;
    }
}
