package javavr.ex4;


public class Ex4Context {
    public final static String JSP_FOLDER = "/WEB-INF/ex4/%s";

    public final static String ACTION_KEY = "action";
    public final static String CODE_KEY = "code";
    public final static String ACTION_DEL_ID = "delById";
    public final static String KEY_ID = "id";
    public final static String ACTION_DEL_ALL = "deleteall";
    public final static String ACTION_ADD_SAMPLE_DATA = "sampledata";
}
