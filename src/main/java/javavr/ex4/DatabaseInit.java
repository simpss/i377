package javavr.ex4;

import javavr.ex4.models.impl.Customer;
import javavr.ex4.models.impl.CustomerType;
import javavr.ex4.util.JpaUtil;
import javax.persistence.EntityManager;

public class DatabaseInit {

    public static void initializeDatabase(){
        EntityManager em = JpaUtil.getFactory().createEntityManager();

        em.getTransaction().begin();

        Customer jane = new Customer();
        jane.setFirstname("Jane");
        jane.setLastname("Doe");
        jane.setCode("123");
        jane.setType(CustomerType.privateCustomer);
        em.merge(jane);
        em.getTransaction().commit();

        em.getTransaction().begin();
        Customer john = new Customer();
        john.setFirstname("john");
        john.setLastname("Doe");
        john.setCode("456");
        john.setType(CustomerType.corporate);
        em.merge(john);
        em.getTransaction().commit();

        em.getTransaction().begin();
        Customer jack = new Customer();
        jack.setFirstname("Jack");
        jack.setLastname("Smith");
        jack.setCode("789");
        jack.setType(CustomerType.unknown);
        em.merge(jack);

        em.getTransaction().commit();
    }
}
