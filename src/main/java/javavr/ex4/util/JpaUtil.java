package javavr.ex4.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
/*
https://bitbucket.org/mkalmo/exjpa/src/b8f1ace0a6228b9daa0a3815ac261355a9c3d9f7/src/main/java/util/?at=master
 */
public class JpaUtil {

    private static EntityManagerFactory emf;

    public static EntityManagerFactory getFactory() {
        if (emf != null) {
            return emf;
        }

        emf = Persistence.createEntityManagerFactory("customerDB");

        return emf;
    }

    public static void closeFactory() {
        if (emf != null) {
            emf.close();
        }
    }

    public static void closeQuietly(EntityManager em) {
        if (em != null) {
            em.close();
        }
    }

}