package javavr.ex4.listeners;

import javavr.ex4.DatabaseInit;
import javavr.ex4.util.JpaUtil;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class StartListener implements ServletContextListener{
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        DatabaseInit.initializeDatabase();
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        JpaUtil.getFactory().close();
    }
}
