package javavr.ex4.servlets;

import javavr.ex4.Ex4Context;
import javavr.ex4.models.CustomerDao;
import javavr.ex4.models.impl.Customer;
import javavr.ex4.models.impl.CustomerDaoImpl;
import javavr.ex4.models.impl.CustomerType;
import javavr.ex4.util.DbUtil;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/Add")
public class Add extends HttpServlet {

    public static final String KEY_FIRST_NAME = "firstname";
    public static final String KEY_SUR_NAME = "surname";
    public static final String KEY_CODE = "code";
    public static final String KEY_CUSTOMER_TYPE = "customerType";

    private static final String JSP_FILE = String.format(Ex4Context.JSP_FOLDER, "Add.jsp");


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (!doAction(req, resp)) {
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(JSP_FILE);
            dispatcher.forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        CustomerDao dao = new CustomerDaoImpl();

        CustomerType type = CustomerType.getFromShittyName(req.getParameter(KEY_CUSTOMER_TYPE));
        Customer customer = new Customer();
        customer.setFirstname(req.getParameter(KEY_FIRST_NAME));
        customer.setLastname(req.getParameter(KEY_SUR_NAME));
        customer.setCode(req.getParameter(KEY_CODE));
        customer.setType(type);

        dao.addCustomer(customer);

        resp.sendRedirect(String.format(Search.SERVLET_URI, req.getContextPath()));
    }

    private boolean doAction(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        CustomerDao dao = new CustomerDaoImpl();
        String action = req.getParameter(Ex4Context.ACTION_KEY);
        if (action != null) {
            if (action.equals(Ex4Context.ACTION_ADD_SAMPLE_DATA)) {
                DbUtil.createTestData();
            } else if (action.equals(Ex4Context.ACTION_DEL_ALL)) {
                dao.deleteAllCustomers();
            } else if (action.equals(Ex4Context.ACTION_DEL_ID)) {
                long id = Long.parseLong(req.getParameter(Ex4Context.KEY_ID));
                dao.deleteCustomer(id);
            }
            resp.sendRedirect(String.format(Search.SERVLET_URI, req.getContextPath()));
            return true;
        }
        return false;
    }
}
