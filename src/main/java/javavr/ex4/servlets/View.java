package javavr.ex4.servlets;

import javavr.ex4.Ex4Context;
import javavr.ex4.models.CustomerDao;
import javavr.ex4.models.impl.CustomerDaoImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/View")
public class View extends HttpServlet{

    private static final String JSP_FILE = String.format(Ex4Context.JSP_FOLDER, "View.jsp");


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String code = req.getParameter(Ex4Context.CODE_KEY);

        if(code == null){
            resp.sendRedirect(String.format(Search.SERVLET_URI, req.getContextPath()));
            return;
        }
        CustomerDao dao = new CustomerDaoImpl();

        req.setAttribute(Ex4Context.KEY_ID, dao.getById(Long.parseLong(code)));

        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(JSP_FILE);
        dispatcher.forward(req, resp);
    }
}
