package javavr.ex4.servlets;

import javavr.ex4.Ex4Context;
import javavr.ex4.models.CustomerDao;
import javavr.ex4.models.impl.Customer;
import javavr.ex4.models.impl.CustomerDaoImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

@WebServlet("/Search")
public class Search extends HttpServlet {

    public static final String SERVLET_URI = "%s/Search";
    public static final String KEY_CUSTOMERS = "customers";
    public static final String KEY_SEARCH = "searchString";
    private static final String JSP_FILE = String.format(Ex4Context.JSP_FOLDER, "Search.jsp");

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ArrayList<Customer> customers = new ArrayList<Customer>();
        CustomerDao dao = new CustomerDaoImpl();

        String searchString = request.getParameter(KEY_SEARCH);

        if (searchString == null || searchString.isEmpty()) {
            customers.addAll(dao.getAllCustomers());
        } else {
            customers.addAll(dao.getFilteredCustomers(searchString));
        }

        request.setAttribute(KEY_CUSTOMERS, customers);
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(JSP_FILE);
        dispatcher.forward(request, response);
    }
}
