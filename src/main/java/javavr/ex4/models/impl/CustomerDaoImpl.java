package javavr.ex4.models.impl;

import javavr.ex4.models.CustomerDao;
import javavr.ex4.util.JpaUtil;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

public class CustomerDaoImpl implements CustomerDao {

    @Override
    public Customer getById(long id) {
        EntityManager em = JpaUtil.getFactory().createEntityManager();
        em.getTransaction().begin();
        return em.find(Customer.class, id);
    }

    @Override
    public ArrayList<Customer> getAllCustomers() {
        EntityManager em = JpaUtil.getFactory().createEntityManager();
        em.getTransaction().begin();

        List<Customer> customers = em.createQuery("SELECT c from Customer c").getResultList();

        return (ArrayList) customers;
    }

    @Override
    public ArrayList<Customer> getFilteredCustomers(String searchString) {
        EntityManager em = JpaUtil.getFactory().createEntityManager();
        em.getTransaction().begin();

        List<Customer> customers = em.createQuery
                ("SELECT c from Customer c where c.firstname like :keyword or c.lastname like :keyword")
                .setParameter("keyword", searchString)
                .getResultList();

        return (ArrayList) customers;
    }

    @Override
    public void deleteCustomer(long id) {
        EntityManager em = JpaUtil.getFactory().createEntityManager();
        em.getTransaction().begin();

        Customer customer = em.find(Customer.class, id);
        if(customer != null){
            em.remove(customer);
        }
        em.getTransaction().commit();
    }

    @Override
    public void addCustomer(Customer customer) {
        EntityManager em = JpaUtil.getFactory().createEntityManager();
        em.getTransaction().begin();

        em.persist(customer);
        em.getTransaction().commit();
    }

    @Override
    public void deleteAllCustomers() {
        EntityManager em = JpaUtil.getFactory().createEntityManager();
        em.getTransaction().begin();
        em.createNativeQuery("DELETE FROM Customer").executeUpdate();
        em.getTransaction().commit();
    }
}
