package javavr.ex4.models.impl;

/**
 * Created by siim on 01/11/14.
 */
public enum CustomerType {
    corporate("customerType.corporate"),
    privateCustomer("customerType.private"),
    unknown("");


    private String shittyName;
    CustomerType(String shittyName){
        this.shittyName = shittyName;
    }

    public String getShittyName(){
        return shittyName;
    }

    public static CustomerType getFromShittyName(String shittyName){
        for(CustomerType e: CustomerType.values()) {
            if(e.getShittyName().equals(shittyName)) {
                return e;
            }
        }
        return null;// not found
    }
}
