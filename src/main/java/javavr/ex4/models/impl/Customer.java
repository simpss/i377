package javavr.ex4.models.impl;

import javax.persistence.*;
import javax.persistence.SequenceGenerator;

@Entity

@SequenceGenerator(name="id_seq", initialValue=1, allocationSize=100)
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "id_seq")
    private long id;
    private String firstname;
    private String lastname;
    private String code;
    private CustomerType type;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public CustomerType getType() {
        if(type == null){
            return CustomerType.unknown;
        }
        return type;
    }

    public void setType(CustomerType type) {
        this.type = type;
    }
}
