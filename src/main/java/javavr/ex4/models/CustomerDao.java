package javavr.ex4.models;

import javavr.ex4.models.impl.Customer;

import java.util.ArrayList;

public interface CustomerDao {

    Customer getById(long id);

    ArrayList<Customer> getAllCustomers();

    ArrayList<Customer> getFilteredCustomers(String searchString);

    void deleteCustomer(long id);

    void addCustomer(Customer customer);

    void deleteAllCustomers();
}
