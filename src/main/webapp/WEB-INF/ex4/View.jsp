<!--this is very ugly, never do it yourself kids.-->

<%@ page import="javavr.ex4.models.impl.Customer" %>
<%@ page import="javavr.ex4.Ex4Context" %>
<%@ page import="javavr.ex4.models.impl.CustomerType" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<body>
<%
    Customer customer = (Customer) request.getAttribute(Ex4Context.KEY_ID);
%>
<ul id="menu">
    <li><a href="<%=request.getContextPath() + "/Search"%>" id="menu_Search">Otsi</a></li>
    <li><a href="<%=request.getContextPath() + "/Add"%>" id="menu_Add">Lisa</a></li>
    <li><a href="<%=String.format("%s/Add?%s=%s",request.getContextPath(), Ex4Context.ACTION_KEY, Ex4Context.ACTION_DEL_ALL)%>" id="menu_ClearData">kustuta k6ik</a></li>
    <li><a href="<%=String.format("%s/Add?%s=%s",request.getContextPath(), Ex4Context.ACTION_KEY, Ex4Context.ACTION_ADD_SAMPLE_DATA)%>" id="menu_InsertData">Sisesta näidisandmed</a></li>
</ul>

<form>
    <table class="formTable" id="formTable">
        <tbody>
        <tr>
            <td>Eesnimi:</td>
            <td><input name="firstName" value="<%=customer.getFirstname()%>" id="firstNameBox" disabled='disabled' /></td>
        </tr>
        <tr>
            <td>Perekonnanimi:</td>
            <td><input name="surname" value="<%=customer.getLastname()%>" id="surnameBox" disabled='disabled'/></td>
        </tr>
        <tr>
            <td>Kood:</td>
            <td><input name="code" value="<%=customer.getCode()%>" id="codeBox" disabled='disabled'/></td>
        </tr>
        <tr>
            <td>Tüüp:</td>
            <td>
                <select id="customerTypeSelect" name="customerType" disabled='disabled'>
                    <%
                        for(CustomerType type : CustomerType.values()){
                            if(type == customer.getType()){
                                %><option value="<%=type.getShittyName()%>" selected="selected"><%=type.getShittyName()%></option><%
                            }else{
                    %>
                            <option value="<%=type.getShittyName()%>"><%=type.getShittyName()%></option>
                    <%
                            }
                        }
                    %>
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="right"><br/>
                <a href="<%=request.getContextPath() + "/Search"%>" id="backLink">Tagasi</a>
            </td>
        </tr>
        </tbody>
    </table>
</form>
</body>
</html>
