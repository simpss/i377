<!--this is very ugly, never do it yourself kids.-->

<%@ page import="javavr.ex4.servlets.Search" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="javavr.ex4.models.impl.Customer" %>
<%@ page import="javavr.ex4.Ex4Context" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="<%= request.getContextPath() + "/resources/stylesheet.css"%>" rel="stylesheet" type="text/css">
    <title>Part 3</title>
</head>
<body>
<%
    ArrayList<Customer> customers = (ArrayList<Customer>) request.getAttribute(Search.KEY_CUSTOMERS);
%>

<ul id="menu">
    <li><a href="<%=request.getContextPath() + "/Search"%>" id="menu_Search">Otsi</a></li>
    <li><a href="<%=request.getContextPath() + "/Add"%>" id="menu_Add">Lisa</a></li>
    <li><a href="<%=String.format("%s/Add?%s=%s",request.getContextPath(), Ex4Context.ACTION_KEY, Ex4Context.ACTION_DEL_ALL)%>" id="menu_ClearData">kustuta k6ik</a></li>
    <li><a href="<%=String.format("%s/Add?%s=%s",request.getContextPath(), Ex4Context.ACTION_KEY, Ex4Context.ACTION_ADD_SAMPLE_DATA)%>" id="menu_InsertData">Sisesta näidisandmed</a></li>
</ul>
<div id="formTable">
    <form method="get" action="<%=request.getContextPath() + "/Search"%>">
        <input name="<%=Search.KEY_SEARCH%>" id="searchStringBox" value=""/>
        <input type="submit" id="filterButton" value="Filtreeri" />
    </form>

    <div id="listTable">
        <table class="listTable">
            <thead>
            <tr>
                <th scope="col">Nimi</th>
                <th scope="col">Perekonnanimi</th>
                <th scope="col">Kood</th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
                <%for(Customer customer : customers) {%>
                    <tr>
                        <div id=row_<%=customer.getCode()%>>
                            <td><a href="<%= String.format("%s/View?code=%s", request.getContextPath(), customer.getId())%>" id="<%=String.format("view_%s", customer.getCode())%>"><%=customer.getFirstname()%></a></td>
                            <td><%=customer.getLastname()%></td>
                            <td><%=customer.getCode()%></td>
                            <td><a href=<%=String.format("\"%s/Add?%s=%s&amp;%s=%s\"", request.getContextPath(), Ex4Context.ACTION_KEY, Ex4Context.ACTION_DEL_ID, Ex4Context.KEY_ID, customer.getId())%> id="delete_<%=customer.getCode()%>">Kustuta</a></td>
                        </div>
                    </tr>
                <%}%>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>
