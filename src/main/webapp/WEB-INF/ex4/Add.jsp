<!--this is very ugly, never do it yourself kids.-->

<%@ page import="javavr.ex4.servlets.Add" %>
<%@ page import="javavr.ex4.models.impl.CustomerType" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<body>
<ul id="menu">
    <li><a href="/Search;jsessionid=175r3mv4x0d1" id="menu_Search">Otsi</a></li>
    <li><a href="/Add;jsessionid=175r3mv4x0d1" id="menu_Add">Lisa</a></li>
    <li><a href="/Admin;jsessionid=175r3mv4x0d1?do=clear_data" id="menu_ClearData">Tühjenda</a></li>
    <li><a href="/Admin;jsessionid=175r3mv4x0d1?do=insert_data" id="menu_InsertData">Sisesta näidisandmed</a></li>
</ul>

<form method="post" action="<%=request.getContextPath() + "/Add"%>">

    <table class="formTable" id="formTable">
        <tbody>
        <tr>
            <td>Eesnimi:</td>
            <td><input name="<%=Add.KEY_FIRST_NAME%>" id="firstNameBox" /></td>
        </tr>
        <tr>
            <td>Perekonnanimi:</td>
            <td><input name="<%=Add.KEY_SUR_NAME%>" id="surnameBox"/></td>
        </tr>
        <tr>
            <td>Kood:</td>
            <td><input name="<%=Add.KEY_CODE%>" id="codeBox"/></td>
        </tr>
        <tr>
            <td>Tüüp:</td>
            <td>
                <select id="customerTypeSelect" name="<%=Add.KEY_CUSTOMER_TYPE%>" >
                    <%
                        for(CustomerType type : CustomerType.values()){
                    %>
                        <option value="<%=type.getShittyName()%>"><%=type.getShittyName()%></option>
                    <%
                        }
                    %>
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="right"><br/>
                <input type="submit" value="Lisa" id="addButton"/>
            </td>
        </tr>
        </tbody>
    </table>
</form>

</body>
</html>
